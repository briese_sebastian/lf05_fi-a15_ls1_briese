import java.util.Scanner;

public class aufgabe2 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		String artikel="";
		int anzahl;
		double preis;
		double mwst;
		double nettogesamtpreis;
		double bruttogesamtpreis;
		
		// Benutzereingaben lesen
		System.out.println("was m�chten Sie bestellen?");
		artikel=liesString(artikel);
		System.out.println("Geben Sie die Anzahl ein:");
		anzahl=liesInt(artikel);
		System.out.println("Geben Sie den Nettopreis ein:");
		preis=liesDouble(artikel);
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		mwst=liesDouble(artikel);

		// Verarbeiten
		nettogesamtpreis=berechneGesamtnettopreis(anzahl,preis);
		bruttogesamtpreis=berechneGesamtbruttopreis(nettogesamtpreis,mwst);

		// Ausgeben
		rechungausgeben(artikel,anzahl,nettogesamtpreis,bruttogesamtpreis,mwst);
		myScanner.close();
	}
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		String Text = myScanner.next();
		return Text;
	}
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		int zahl = myScanner.nextInt();
		return zahl;
	}
	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		double zahl = myScanner.nextDouble();
		return zahl;
	}
	public static double berechneGesamtnettopreis(int anzahl, double
	nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis,
	double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	public static void rechungausgeben(String artikel, int anzahl, double
	nettogesamtpreis, double bruttogesamtpreis,
	double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}

}
