import java.util.Scanner;

public class mathe {

	public static void main(String[] args) {
		Scanner mathscanner= new Scanner(System.in);
		System.out.println("welche Zahl willst du quadrieren?");
		double x=mathscanner.nextDouble();
		double y=quadrat(x);
		System.out.println(x+"� = "+y);
		
		System.out.println("Kathete 1 angeben:");
		double k1=mathscanner.nextDouble();
		System.out.println("Kathete 2 angeben:");
		double k2=mathscanner.nextDouble();
		double hypotenuse=hypotenuse(k1,k2);
		System.out.printf("Die Hypotenuse ist "+"%.2f"+" lang.",hypotenuse);
		
		mathscanner.close();
		
	}
	public static double quadrat(double x) {
		double y;
		y=x*x;
		return y;
	}
	public static double hypotenuse(double kathete1,double kathete2) {
		double h= Math.sqrt(quadrat(kathete1)+quadrat(kathete2));
		return h;
	}
}
