
public class aufgabe4 {

	public static void main(String[] args) {
		double Wa,WV; Wa=5;
		WV=VolumenW(Wa);
		System.out.println("Würfelvolumen: "+Wa+"³="+WV);
		double Qa,Qb,Qc,QV;
		Qa=1;Qb=2;Qc=3; 
		QV=VolumenQ(Qa,Qb,Qc);
		System.out.println("Quadervolumen: "+Qa+"*"+Qb+"*"+Qc+"="+QV);
		double Pa,Ph,PV;
		Pa=4;Ph=6;
		PV=VolumenP(4,6);
		System.out.println("Pyramidenvolumen: "+Pa+"² * "+Ph+"/3 ="+PV);
		double Kr,KV;
		Kr=6.9;
		KV=VolumenK(Kr);
		System.out.println("Kugelvolumen: 4/3 * "+Kr+"³ * PI ="+KV);
		

	}
	public static double VolumenW(double a) {
		double V;
		V=a*a*a;
		return V;
	}
	public static double VolumenQ(double a,double b,double c) {
		double V;
		V=a*b*c;
		return V;
	}
	public static double VolumenP(double a,double h) {
		double V;
		V=a*a*h/3;
		return V;
	}
	public static double VolumenK(double r) {
		double V;
		V=(4.0/3.0)*(r*r*r)*Math.PI;
		return V;
		
	}
}
