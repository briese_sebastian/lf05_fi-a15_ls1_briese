﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args){
       
       double zuzahlenderBetrag; 
       double rückgabebetrag;
       double ticketpreis=0;
       int ticketanzahl=0;
       
       // Bestellung
       zuzahlenderBetrag=fahrkartenbestellungErfassen(ticketpreis,ticketanzahl);

       // Geldeinwurf
       rückgabebetrag = fahrkartenBezahlen(zuzahlenderBetrag);
      
       // Fahrscheinausgabe
       fahrkartenAusgeben();
    
       //Restgeldausgabe
       rueckgeldausgabe(rückgabebetrag);
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
}
	public static double fahrkartenbestellungErfassen(double tp, int ta) {
		Scanner tastatur = new Scanner(System.in);
		double zzb;
		System.out.println("Ticketpreis:");
		tp=tastatur.nextDouble();
		System.out.println("Ticketanzahl:");
		ta=tastatur.nextInt();
		zzb=tp*ta;
		System.out.println("zu zahlender Betrag: "+zzb);
		return zzb;
	}
	public static double fahrkartenBezahlen(double zzB) {
		Scanner tastatur = new Scanner(System.in);
		 double eingezahlterGesamtbetrag = 0.0;
		 double nochoffen;
		 double eingeworfeneMünze;
		 double rückgabe;
	       while(eingezahlterGesamtbetrag < zzB)
	       {
	    	   nochoffen=zzB-eingezahlterGesamtbetrag;
	    	   System.out.printf("Noch zu zahlen: " + "%.2f" + " Euro" ,nochoffen);
	    	   System.out.print("\nEingabe (mind. 1Ct, höchstens 10 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;}
	       rückgabe=eingezahlterGesamtbetrag-zzB;
	       return rückgabe;
	       }
	public static void fahrkartenAusgeben() {
		   System.out.println("\nFahrschein wird ausgegeben");
	       for(int i=0;i<8;i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	public static void rueckgeldausgabe(double ruckgabe) {
		if(ruckgabe > 0.0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f" + " EURO" ,ruckgabe);
	    	   System.out.println("\nwird in folgenden Münzen ausgezahlt:");
	    	   
	    	   while(ruckgabe >= 10.0) // 10 EURO-Schein
	           {
	        	  System.out.println("10 EURO");
		          ruckgabe -=10.0;
	           }
	    	   while(ruckgabe >= 5.0) // 5 EURO-Schein
	           {
	        	  System.out.println("5 EURO");
		          ruckgabe -= 5.0;
	           }
	           while(ruckgabe >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          ruckgabe -= 2.0;
	           }
	           while(ruckgabe >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          ruckgabe -= 1.0;
	           }
	           while(ruckgabe >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          ruckgabe -= 0.5;
	           }
	           while(ruckgabe >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          ruckgabe -= 0.2;
	           }
	           while(ruckgabe >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          ruckgabe -= 0.1;
	           }
	           while(ruckgabe >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          ruckgabe -= 0.05;
	           }
	           while(ruckgabe >= 0.02) // 2 CENT-Münzen
	           {
	        	  System.out.println("2 CENT");
		          ruckgabe -= 0.02;
	           }
	           while(ruckgabe >= 0.01) // 1 CENT-Münzen
	           {
	        	  System.out.println("1 CENT");
		          ruckgabe -= 0.01;
	           }
		
	}
}}
//5."ticketpreis" und "nochoffen" sind double um Centbeträge zu erfassen und "ticketanzahl" ist int ,da Tickets nur in Ganzzahlschritten gekauft werden.
//6. Es wird das Produkt von anzahl*einzelpreis gebildet.