//@sebastian briese
//@05.10.21
public class Variablen {

	public static void main(String[] args) {
		 /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
        Vereinbaren Sie eine geeignete Variable */
		int zaehler;
  /* 2. Weisen Sie dem Zaehler den Wert 25 zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		zaehler=25;
		System.out.println(zaehler);
  /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
        eines Programms ausgewaehlt werden.
        Vereinbaren Sie eine geeignete Variable */
		char buchstabe;

  /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		buchstabe='C';
		System.out.println(buchstabe);
  /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
        notwendig.
        Vereinbaren Sie eine geeignete Variable */
		double grossezahl;

  /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
        und geben Sie sie auf dem Bildschirm aus.*/
		grossezahl=299790000;
		System.out.println("c="+grossezahl+"m/s");
  /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
        soll die Anzahl der Mitglieder erfasst werden.
        Vereinbaren Sie eine geeignete Variable und initialisieren sie
        diese sinnvoll.*/
		byte vereinboys;vereinboys=7;

  /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
		System.out.println(vereinboys);
  /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
        Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
        dem Bildschirm aus.*/
		double ladung;ladung=1.602176634E-19;
		System.out.println(ladung);

  /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
        Vereinbaren Sie eine geeignete Variable. */
		boolean zahlungerfolgt;

  /*11. Die Zahlung ist erfolgt.
        Weisen Sie der Variable den entsprechenden Wert zu
        und geben Sie die Variable auf dem Bildschirm aus.*/
		zahlungerfolgt=true;
		System.out.println(zahlungerfolgt);
}//main
}// Variablen