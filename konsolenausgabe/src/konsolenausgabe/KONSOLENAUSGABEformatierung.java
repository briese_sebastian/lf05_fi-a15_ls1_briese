package konsolenausgabe;

public class KONSOLENAUSGABEformatierung {

	public static void main(String[] args) {
		String f = "**";
		System.out.printf(""
				+ "\n%5s"
				+ "\n%.1s%7.1s"
				+ "\n%.1s%7.1s"
				+ "\n%5s "
				,f,f,f,f,f,f);
		
		int g= 0, h= 1, i= 2, j= 3, k= 4, l= 5, n= 6, o= 24, p=120 ;
		String m= "=";
		System.out.printf("\n"
				+ "\n%-1d!%4s%20s%4d"
				+ "\n%-1d!%4s%2s%18s%4d"
				+ "\n%-1d!%4s%2s * %1s%14s%4d"
				+ "\n%-1d!%4s%2s * %1s * %1s%10s%4d"
				+ "\n%-1d!%4s%2s * %1s * %1s * %1s%6s%4d"
				+ "\n%-1d!%4s%2s * %1s * %1s * %1s * %1s%2s%4d"
		,g,m,m,h
		,h,m,h,m,h
		,i,m,h,i,m,i
		,j,m,h,i,j,m,n
		,k,m,h,i,j,k,m,o
		,l,m,h,i,j,k,l,m,p);
		
		int f1=-20,f2=-10,f3=0,f4=20,f5=30;
		double c1=-28.8889,c2=-23.3333,c3=-17.7778,c4=-6.6667,c5=-1.1111;
		System.out.printf("\n"
				+ "\nFahrenheit  |   Celsius"
				+ "\n-----------------------"
				+ "\n%-12d|%10.2f"
				+ "\n%-12d|%10.2f"
				+ "\n+%-11d|%10.2f"
				+ "\n+%-11d|%10.2f"
				+ "\n+%-11d|%10.2f"
				+ "\n"
				,f1,c1,f2,c2,f3,c3,f4,c4,f5,c5);
}}